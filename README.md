# Renovate Bot Config

Renovate Bot configuration for 4s1 projects on Codeberg.

## How to use in a project

Create a `.renovaterc.json` file in the root folder of your project and add:

```json
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": ["local>4s1/renovate-config"]
}
```
